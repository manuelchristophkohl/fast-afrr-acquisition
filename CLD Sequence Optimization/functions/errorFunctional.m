
% Filename : errorFunctional.m
% Date     : 20.02.2017
% Author   : Manuel C. Kohl

function error = errorFunctional(ISIsequence)

    global samplingRate;
    global frequencyWindow;
    global targetISI;

    % Calculating noise amplification
    
    trigger = sequenceToTrigger(round(ISIsequence*samplingRate));
    NAF = noiseAmplificationFactor(trigger);
    
    % Punishing noise amplification in frequency window of interest and
    % deviation from target ISI

    frequencies = linspace(0, samplingRate/2, length(NAF));
    mask = 1*(frequencies > frequencyWindow(1)) .* ...
        (frequencies < frequencyWindow(end));
    windowedNAF = NAF(mask == 1);
    error = norm(windowedNAF(windowedNAF > 1))^2; % + norm((ISIsequence-targetISI)/targetISI)^2;
    
end
