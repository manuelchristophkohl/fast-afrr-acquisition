
% Filename : differentialEvolution.m
% Date     : 17.09.2018
% Authors  : Manuel C. Kohl

function minimumParameters = differentialEvolution(functional, ...
    nParameters, lowerParameterBounds, upperParameterBounds, ...
    populationSize, scalingFactorBounds, crossoverProbability, ...
    tolerance, nMaximumIterations, showProgressFigure)

    % Initializing random population

    populationParameters = zeros(populationSize, nParameters);
    
    for k = 1:nParameters
    
        populationParameters(:, k) = unifrnd(lowerParameterBounds(k), ...
            upperParameterBounds(k), [populationSize 1]);
        
    end
    
    functionalValues = inf(1, populationSize);
    bestParameterIndex = inf;
    
    for k = 1:populationSize
        
        functionalValues(k) = functional(populationParameters(k, :));
        
        if functionalValues(k) < bestParameterIndex
            
            bestParameterIndex = k;
            
        end        
        
    end
     
    % Performing iterative parameter optimization
    
	functionalValueProgression = NaN(1, nMaximumIterations);
    fprintf('Performing differential evolution on functional F.\n');
	if showProgressFigure
		figure;
	end
    n = 0;

    while true

        n = n + 1;

        for k = 1:populationSize
            
            % Selecting random individals for recombination

            selectedParameters = populationParameters(k, :);
            randomPopulationOrder = randperm(populationSize);
            randomPopulationOrder(randomPopulationOrder == k) = [];

            % Performing parameter recombination

            scalingFactors = unifrnd(scalingFactorBounds(1), ...
                scalingFactorBounds(end), [1 nParameters]);
            recombinedParameters = populationParameters(randomPopulationOrder(1), :) + ...
                scalingFactors .* (populationParameters(randomPopulationOrder(2), :) - ...
                populationParameters(randomPopulationOrder(3), :));
            
            % Enforcing boundary conditions
            
            recombinedParameters = max(recombinedParameters, lowerParameterBounds);
            recombinedParameters = min(recombinedParameters, upperParameterBounds);

            % Performing parameter crossover

            crossoverParameters = zeros(1, nParameters);
            randomParameterIndex = randi([1 nParameters]);

            for j = 1:nParameters
                
                if (j == randomParameterIndex) || (rand(1) <= crossoverProbability)
                    
                    crossoverParameters(j) = recombinedParameters(j);
                    
                else
                    
                    crossoverParameters(j) = selectedParameters(j);
                    
                end
                
            end
			
			% Performing selection of candidate parameters
            
            candidateParameters = crossoverParameters;
            candidateFunctionalValue = functional(crossoverParameters);

            if candidateFunctionalValue < functionalValues(k)
                
                populationParameters(k, :) = candidateParameters;
                functionalValues(k) = candidateFunctionalValue;
                
                if functionalValues(k) < functionalValues(bestParameterIndex)
                
                    bestParameterIndex = k;
                    
                end
                
            end

        end

        % Displaying updated functional value

        functionalValueProgression(n) = functionalValues(bestParameterIndex);    
        fprintf('Iteration %i, F = %d ...\n', n, functionalValueProgression(n));
		if showProgressFigure
			semilogy(1:n, functionalValueProgression(1:n));
			grid on;
			title('Differential evolution on functional F');
			xlabel('Iterations n');
			ylabel('Functional value F(n)');
			getframe;  
		end

        % Aborting if stopping criteria are met

        if functionalValueProgression(n) < tolerance
            
            fprintf('Tolerance reached, aborting.\n');
            break;
            
        end
        
        if n == nMaximumIterations
            fprintf('Maximum number of iterations reached, aborting.\n');
            break;        
        end    

    end
    
    % Returning minimum parameters
    
    minimumParameters = populationParameters(bestParameterIndex, :);

end
