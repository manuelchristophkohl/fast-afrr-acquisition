
% Filename : displayCLDsequenceNAF.m
% Date     : 19.10.2016
% Author   : Manuel C. Kohl

function [] = displayCLDsequenceNAF(sequenceFilename, samplingRate, frequencyWindow, varargin)

	nVarargs = length(varargin);
    for k = 1:nVarargs-1
        if strcmp(varargin{k}, 'frequencyAxis')
                frequencyAxis = varargin{k+1};
        elseif strcmp(varargin{k}, 'magnitudeAxis')
                magnitudeAxis = varargin{k+1};
        end       
    end
    if ~exist('frequencyAxis', 'var')
        frequencyAxis = 'logarithmic';
    end
    if ~exist('magnitudeAxis', 'var')
        magnitudeAxis = 'dB';
    end

    % Load CLD sequence from file and calculate NAF
    
    load([sequenceFilename '.mat']);
    CLDsequence = round(CLDsequence * samplingRate);
    NAF = noiseAmplificationFactor(sequenceToTrigger(CLDsequence));
    nSamples = length(NAF);
    frequencies = linspace(0, samplingRate/2, nSamples);
    
    % Display NAF
    
    figure;
    hold on;
    switch magnitudeAxis
        case 'linear'
            patch([min(frequencyWindow), min(frequencyWindow), max(frequencyWindow), ...
                max(frequencyWindow)], [0, 2, 2, 0], 1, 'FaceColor', [0.75 0.75 0.75], ...
                'FaceAlpha', 0.5, 'EdgeColor', 'none');
            plot(frequencies, NAF);
            plot(frequencies, ones(1, nSamples));
            ylim([0, 2]);
            ylabel('NAF [1]');
        case 'dB'
            patch([min(frequencyWindow), min(frequencyWindow), max(frequencyWindow), ...
                max(frequencyWindow)], [min(20*log10(NAF))-6, max(20*log10(NAF))+6, ...
                max(20*log10(NAF))+6, min(20*log10(NAF))-6], 1, 'FaceColor', [0.75 0.75 0.75], ...
                'FaceAlpha', 0.5, 'EdgeColor', 'none');
            plot(frequencies, 20*log10(NAF));
            plot(frequencies, zeros(1, nSamples));
            ylim([min(20*log10(NAF))-6 max(20*log10(NAF))+6]);
            ylabel('NAF [dB]');
    end
    hold off;
    if strcmp(frequencyAxis, 'logarithmic')
        set(gca, 'xscale', 'log');
    end
    grid on;
    xlim([0.5*frequencyWindow(1), 2*frequencyWindow(end)]);
    xlabel('Frequency f [Hz]');
    title('CLD sequence NAF');
    legend({'Frequency window', 'CLD sequence NAF', 'Averaging equivalent NAF'});

end
