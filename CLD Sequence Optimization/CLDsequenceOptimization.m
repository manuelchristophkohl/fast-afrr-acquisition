
% Filename : CLDsequenceOptimization.m
% Date     : 23.10.2018
% Author   : Manuel C. Kohl

clc;
clear;
close all;

addpath(genpath('./'));

fprintf('[CLD sequence optimization]\n');

% Parameters

audioRate = 48000;                      % Sampling rate used in audio playback system             
EEGrate = 19200;                        % Sampling rate used in EEG acquisition system
nStimuli = 16;                          % Number of stimuli in CLD sequence
global targetISI;
targetISI = 1/40;
lowerISIbound = 1.0*targetISI*ones(1, nStimuli);
upperISIbound = 1.6*targetISI*ones(1, nStimuli); 
global frequencyWindow;
frequencyWindow = [20 750];             % Frequency window to minimize noise amplification
populationSize = 25;					% Size of population used for differential evolution
scalingFactorBounds = [0.2 0.8];	    % Scaling factor bounds in differential evolution
crossoverProbability = 0.2; 			% Probability for crossover in differential evolution
tolerance = 1e-2;						% Error tolerance in differential evolution
nMaximumIterations = 1e2;				% Maximum number of differential evolution iterations

% Performing differential evolution to find optimum CLD sequence

global samplingRate;
samplingRate = gcd(audioRate, EEGrate);
CLDsequence = differentialEvolution(@errorFunctional, nStimuli, ...
	lowerISIbound, upperISIbound, populationSize, scalingFactorBounds, ...
    crossoverProbability, tolerance, nMaximumIterations, true);

% Saving optimized CLD sequence

fprintf('Saving optimized CLD sequence to file ... ');
sequenceFilename = sprintf('CLD_sequence_%i_stimuli_ISI_%4.3f-%4.3f_s_frequencyWindow_%d-%d_Hz', ...
    nStimuli, min(CLDsequence), max(CLDsequence), frequencyWindow(1), frequencyWindow(end));
save(['sequences/' sequenceFilename '.mat'], 'CLDsequence');
fprintf('Done.\n');

% Displaying optimized CLD sequence properties

fprintf('Displaying optimized CLD sequence properties ... ');
displayCLDsequenceProperties(sequenceFilename, samplingRate, frequencyWindow);
fprintf('Done.\n');
