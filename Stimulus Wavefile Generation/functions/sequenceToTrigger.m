
% Filename : sequenceToTrigger.m
% Date     : 18.10.2016
% Author   : Manuel C. Kohl

function trigger = sequenceToTrigger(ISIsequence)

    nSamples = sum(ISIsequence);
    trigger = zeros(1, nSamples);
    trigger(cumsum(ISIsequence)+1) = 1;
    trigger(1) = 1;
    trigger = trigger(1:end-1);

end