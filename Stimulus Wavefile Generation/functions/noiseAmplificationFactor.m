
% Filename : noiseAmplificationFactor.m
% Date     : 13.08.2016
% Author   : Manuel C. Kohl

function amplificationFactor = noiseAmplificationFactor(trigger)

    % Calculating FFT magnitudes

    nFFTsamples = round(length(trigger)/2)+1;
    triggerFFTabs = abs(fft(trigger));
    triggerFFTabs = triggerFFTabs(1:nFFTsamples);
    
    % Returning noise amplification factor
      
    amplificationFactor = 1./(triggerFFTabs);
    
end