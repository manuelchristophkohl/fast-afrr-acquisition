
% Filename : displayCLDsequenceRateHistogram.m
% Date     : 24.02.2016
% Author   : Manuel C. Kohl

function [] = displayCLDsequenceRateHistogram(sequenceFilename)

    % Load CLD sequence from file and calculate ISI histogram
    
    load([sequenceFilename '.mat']);
    nISI = length(CLDsequence);
    rates = 1./CLDsequence;
    [ISIprevalence, ISIbins] = hist(rates, nISI);
    
    % Display ISI histogram
    
    figure;
    bar(ISIbins, ISIprevalence);
    xlim([min(rates) max(rates)]);
    grid on;
    xlabel('Rate [Hz]');
    ylabel('Prevalence [1]');
    title('CLD sequence rate histogram');

end
