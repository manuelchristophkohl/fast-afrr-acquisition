
% Filename    : whiteNoise.m
% Date        : 02.01.2016
% Author      : Manuel C. Kohl

% Description : This function generates a sequence of white (uniform) noise 
%               samples. In terms of power at a constant bandwidth, 
%               white noise is constant across the spectrum. 

function noise = whiteNoise(nSamples)

    % Generate white noise

    noise = rand(1, nSamples);
    
    % Remove DC and map to [-1..1]

    noise = noise - mean(noise);
    noise = noise / max(abs(noise));

end
