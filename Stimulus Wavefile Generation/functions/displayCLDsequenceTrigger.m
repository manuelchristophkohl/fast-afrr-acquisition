
% Filename : displayCLDsequenceTrigger.m
% Date     : 21.02.2016
% Author   : Manuel C. Kohl

function [] = displayCLDsequenceTrigger(sequenceFilename, samplingRate)

    % Load CLD sequence from file and calculate trigger
    
    load([sequenceFilename '.mat']);
    CLDsequence = round(CLDsequence * samplingRate);
    trigger = sequenceToTrigger(CLDsequence);
    trigger(trigger == 0) = NaN;
    nSamples = length(trigger);
    time = linspace(0, nSamples/samplingRate, nSamples);
    
    % Display trigger
    
    figure;
    stem(time, trigger);
    grid on;
    xlim([min(time) max(time)]);
    ylim([0 1]);
    xlabel('Time t [s]');
    set(gca, 'YTick', [], 'YTickLabel', []);
    title('CLD sequence trigger');

end
