
% Filename : displayCLDsequenceProperties.m
% Date     : 24.10.2018
% Author   : Manuel C. Kohl

function [] = displayCLDsequenceProperties(sequenceFilename, samplingRate, frequencyWindow)

    % Displaying optimum CLD sequence properties

    displayCLDsequenceTrigger(sequenceFilename, samplingRate);
    displayCLDsequenceRateHistogram(sequenceFilename);
    displayCLDsequenceNAF(sequenceFilename, samplingRate, frequencyWindow, 'magnitudeAxis', 'dB');
    ylim([-24, 6]);
    
end
