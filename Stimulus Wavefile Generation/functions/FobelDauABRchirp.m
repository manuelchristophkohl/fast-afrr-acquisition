
% Filename : FobelDauABRchirp.m
% Authors  : Oliver Fobel | Thorsten Dau | Farah I. Corona-Strauss | Manuel C. Kohl
% Date     : 22.06.2017

% Description : Flat-spectrum intensity-dependent chirp optimized for
%               eliciting the ABR in humans, as proposed by Fobel & Dau 2004

function chirp = FobelDauABRchirp(fl, fu, fs, intensity)

    b = 12.9;  % Neely et al. 1988
    c = 5.0;   % Neely et al. 1988
    d = 0.413; % Neely et al. 1988
    g = intensity / 1e2; 
    fmin = fl / 1e3;
    fmax = fu / 1e3;
    t0 = b*c^(-g)*(fmin)^(-d);
    t1 = t0-b*c^(-g)*(fmax)^(-d);
    c1 = (1/d) - 1;
    c2 = (2*pi*(b*c^(-g))^(1/d))/c1;
    t = linspace(0, t1, round(t1/1e3*fs));
    amp = sqrt((b*c^(-g))^(1/d)./(d*(t0-t).^(1/d+1)));
    phi = c2*((1./((t0-t).^c1))-(1/(t0^c1)));
    chirp = amp .* sin(phi);
    chirp = chirp / max(abs(chirp));

end