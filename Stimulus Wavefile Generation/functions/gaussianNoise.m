
% Filename    : gaussianNoise.m
% Date        : 13.03.2017
% Author      : Manuel C. Kohl

% Description : This function generates a sequence of gaussian
%               noise samples.

function noise = gaussianNoise(nSamples)

    % Generate gaussian noise

    noise = randn(1, nSamples);
    
    % Remove DC and map to [-1..1]

    noise = noise - mean(noise);
    noise = noise / max(abs(noise));

end
