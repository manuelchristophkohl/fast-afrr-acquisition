
% Filename    : violetNoise.m
% Date        : 02.01.2016
% Author      : Hristo Zhivomirov | Manuel C. Kohl

% Description : This function generates a sequence of violet (f^2) noise 
%               samples. In terms of power at a constant bandwidth,
%               violet noise increases at 6 dB per octave. 

function noise = violetNoise(nSamples)

    % Ensure that FFT length is even
    if rem(nSamples,2)
        nFFTsamples = nSamples+1;
    else
        nFFTsamples = nSamples;
    end

    % Generate white noise
    noise = whiteNoise(nFFTsamples);

    % Perform FFT
    noiseFFT = fft(noise);

    % Generate weight vector
    NumUniquePts = nFFTsamples/2 + 1;
    weight = 1:NumUniquePts;

    % Multiply the left half of the spectrum so the power spectral density
    % is proportional to the frequency by factor f^2 (i.e. the magnitudes
    % are proportional to f), then mirror to the right half excluding the
    % unique bins (DC component and Nyquist frequency)
    noiseFFT(1:NumUniquePts) = noiseFFT(1:NumUniquePts) .* weight;
    noiseFFT(NumUniquePts+1:nFFTsamples) = real(noiseFFT(nFFTsamples/2:-1:2)) ...
        - 1i*imag(noiseFFT(nFFTsamples/2:-1:2));

    % Perform IFFT and crop real part to requested length
    noise = ifft(noiseFFT);
    noise = real(noise(1, 1:nSamples));

    % Remove DC and map to [-1..1]

    noise = noise - mean(noise);
    noise = noise / max(abs(noise));

end
