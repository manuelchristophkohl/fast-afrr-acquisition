
% Filename : zeroPhaseButterworthBandpass.m
% Date     : 28.06.2016
% Author   : Manuel C. Kohl

function [filteredSignals] = zeroPhaseButterworthBandpass(signals, samplingRate, cutoffs, order)

    [b, a] = butter(order, 2*cutoffs/samplingRate, 'bandpass'); 
    % fvtool(b, a);
	filteredSignals = zeros(size(signals));
	for k = 1:size(signals, 1)
		filteredSignals(k, :) = filtfilt(b, a, signals(k, :));
	end

end