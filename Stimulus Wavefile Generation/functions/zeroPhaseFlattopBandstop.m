
% Filename : zeroPhaseFlattopBandstop.m
% Date     : 07.06.2016
% Author   : Manuel C. Kohl

function [filteredSignals] = zeroPhaseFlattopBandstop(signals, samplingRate, cutoffs, order)

    window = flattopwin(order+1);
    b = fir1(order, 2*cutoffs/samplingRate, 'stop', window, 'scale');
    % fvtool(b, 1);
	filteredSignals = zeros(size(signals));
	for k = 1:size(signals, 1)
		filteredSignals(k, :) = filtfilt(b, 1, signals(k, :));
	end
    
end