
% Filename : zeroPhaseButterworthNotch.m
% Date     : 07.06.2016
% Author   : Manuel C. Kohl

function [filteredSignals] = zeroPhaseButterworthNotch(signals, samplingRate, centerFrequency, octaveBandwidth)

    % Get minimum and maximum cutoff frequencies for the notch filter [Tietze & Schenk, p. 413]
    fBandEdges = [0.5*(sqrt(octaveBandwidth^2+4)-octaveBandwidth), ...
        0.5*(sqrt(octaveBandwidth^2+4)+octaveBandwidth)]*centerFrequency;

    [b, a] = butter(2, 2*fBandEdges/samplingRate, 'stop'); 
    % fvtool(b, a);
	filteredSignals = zeros(size(signals));
	for k = 1:size(signals, 1)
		filteredSignals(k, :) = filtfilt(b, a, signals(k, :));
	end

end