
% Filename : stimulusWavefileGeneration.m
% Date     : 24.10.2018
% Author   : Manuel C. Kohl

clc;
clear;
close all;
addpath(genpath('./'));

fprintf('[Stimulus wavefile generation]\n');

% Parameters

samplingRate = 48000;                       % Generate 48kHz/32bit PCM WAVE @ 0dB FS (peak)
bitDepth = 32;
stimulusType = 'broadbandChirp';

switch stimulusType

    case 'broadbandChirp'
        chirpRange = [1e2 1e4];             % Broadband, flat-spectrum chirp optimized for eliciting the
        chirpIntensity = 65;                % ABR in humans at 65 dB peSPL as described by Fobel & Dau 2004
        
    case 'bandlimitedChirp'   
        chirpRange = [1e2 584];             % Bandlimited, flat-spectrum chirp optimized for eliciting the
        chirpIntensity = 65;                % ABR in humans at 65 dB peSPL as described by Fobel & Dau 2004, containing
                                            % 3 half-waves with zero-amplitude borders according to Wegner & Dau 2002, p. 3
                                                    
    case 'toneBurst'  
        toneFrequency = 5e2;                % Toneburst with 2/1/2 rise/plateau/fall time ratios, 5 periods of a 500 Hz pure tone,
        nPeriods = 5;                       % described in Hall 2006, p. 65f, used by Davis et al. 1985, p. 1 and Stapells 1994, p. 2
        tapers = 0.8;                       % for frequency-specific / low-frequency ABR, windowed using square cosine tapers
                                            % as suggested by Neely et al. 1988, p. 2
                                            
    case 'click'    
        pulseWidth = 1e-4;                  % Click duration of 100 �s according to Hall 2006
        polarity = 1;                       % Click polarity, condensation [1] or rarefaction [-1]
        
end

noiseType = 'pinkNoise';					% Spectral property of noise used for masking
noiseRange = [1e2 1e4];                     % Broadband masking noise range [Hz] as proposed by Humes et al. 1982, p. 1
fadeTime = 2;                               % Fade-in and fade-out duration of masking noise [s]

switch stimulusType    
    case 'bandlimitedChirp'
        bandstopRange = chirpRange;         % Notched masking noise generated with a FIR bandstop, see Corona-Strauss et al. 2012, p. 3  
        
    case 'toneBurst'
        notchBandwidth = 1;                 % Notched masking noise generated with an IIR octave notch, see Stapells et al. 1990, p. 1
        
end                                  

ITD = 1e-3*[0 0.63];                        % Interaural time differences (ITD) [s] to be generated
alternatePolarity = true;                   % Polarity alternation on stimulus presentation
nSeries = 100;                              % Number of stimulus series (structure : CLD, IBI, ISO, IBI)
CLDtriggerDuration = 1e-3;                  % Trigger duration in [s] (CLD sequence presentations)
ISOtriggerDuration = 1e-2;                  % Trigger duration in [s] (ISO stimuli presentations)
nISOrepetitions = 1;                        % Number of ISO stimulus presentations per series
nCLDrepetitions = 1;                        % Number of net CLD sequence repetitions per series
nInitialCLDrepetitions = 1;                 % Number of CLD sequence repetitions prior to first acquisition trigger
ISI = 0.5;                                  % Mean interstimulus interval (ISI) of ISO stimuli presentations in [s]
ISIrandomAmount = 0;                        % Amount of equally distributed ISI jitter [0 .. 0.5[
IBI = 2;                                    % Mean interblock interval (IBI) between CLD and ISO presentations in [s]
IBIrandomAmount = 0.2;                      % Amount of equally distributed IBI jitter [0 .. 0.5[

CLDsequenceFilename = 'S1_CLD_sequence_16_stimuli_ISI_0.025-0.043_s_frequencyWindow_20-750_Hz';
frequencyWindow = [20 800];                % Frequency window of CLD sequence optimum NAF

% Generate stimulus signal

switch stimulusType
    case {'broadbandChirp', 'bandlimitedChirp'}
        stimulusSignal = FobelDauABRchirp(chirpRange(1), chirpRange(end), samplingRate, chirpIntensity);
        nStimulusSignalSamples = length(stimulusSignal);
        stimulusLength = nStimulusSignalSamples/samplingRate;
        stimulusTimebase = linspace(0, stimulusLength, nStimulusSignalSamples);
    case 'toneBurst'
        stimulusLength = nPeriods/toneFrequency;
		stimulusTimebase = 0:1/samplingRate:stimulusLength;
		nStimulusSignalSamples = length(stimulusTimebase);
        stimulusSignal = -cos(2*pi*toneFrequency*stimulusTimebase);
        window = (tukeywin(nStimulusSignalSamples, tapers)').^2;
        stimulusSignal = stimulusSignal .* window;
    case 'click'
        stimulusLength = pulseWidth;
		stimulusTimebase = 0:1/samplingRate:stimulusLength;
        nStimulusSignalSamples = length(stimulusTimebase);
        stimulusSignal = polarity*ones(1, nStimulusSignalSamples);
end

fprintf('Stimulus length : %1.3f ms\n', stimulusLength*1e3);

% Load CLD sequence from file and display properties

load([CLDsequenceFilename, '.mat']);
displayCLDsequenceProperties(CLDsequenceFilename, samplingRate, frequencyWindow);
nCLDsequenceStimuli = length(CLDsequence);

% Generate trigger track and stimulus train

nStimuli = nSeries*((nInitialCLDrepetitions+nCLDrepetitions)*nCLDsequenceStimuli + nISOrepetitions);
presentationTimes = zeros(1, nStimuli);
triggerDurations = zeros(1, nStimuli);
currentTime = fadeTime;
currentIndex = 1;
for k = 1:nSeries
    for l = 1:(nInitialCLDrepetitions+nCLDrepetitions)
        for m = 1:nCLDsequenceStimuli
            presentationTimes(currentIndex) = currentTime;
            triggerDurations(currentIndex) = (l > nInitialCLDrepetitions)*(m == 1)*CLDtriggerDuration;
            currentTime = currentTime + CLDsequence(m);
            currentIndex = currentIndex + 1;
        end
    end
    if nISOrepetitions > 0
        currentTime = currentTime + IBI*(1 + 2*IBIrandomAmount*(0.5-rand(1)));
    end
    for l = 1:nISOrepetitions
        presentationTimes(currentIndex) = currentTime;
        triggerDurations(currentIndex) = ISOtriggerDuration;
        if nISOrepetitions > 1
            currentTime = currentTime + ISI*(1 + 2*ISIrandomAmount*(0.5-rand(1)));
        end
        currentIndex = currentIndex + 1;
    end
    currentTime = currentTime + IBI*(1 + 2*IBIrandomAmount*(0.5-rand(1)));
end
nStimulusTrainSamples = round((presentationTimes(end) + IBI + fadeTime)*samplingRate);
stimulusTrain = zeros(1, nStimulusTrainSamples);
triggerTrack = zeros(1, nStimulusTrainSamples);
for k = 1:nStimuli
    iLow = round(presentationTimes(k)*samplingRate);
    stimulusTrain(iLow:iLow+nStimulusSignalSamples-1) = (-1)^(alternatePolarity*k)*stimulusSignal;
    triggerTrack(iLow:iLow+round(triggerDurations(k)*samplingRate)-1) = 1;
end
nITD = length(ITD);
delayedStimulusTrain = zeros(nITD, nStimulusTrainSamples);
for k = 1:nITD
	delayedStimulusTrain(k, :) = circshift(stimulusTrain, round(ITD(k)*samplingRate), 2);
end

% Generate silence and masking noise(s), applying fades

silence = zeros(1, nStimulusTrainSamples);
switch noiseType
	case 'violetNoise'
		maskingNoise = violetNoise(nStimulusTrainSamples);
	case 'blueNoise'
		maskingNoise = blueNoise(nStimulusTrainSamples);
	case 'whiteNoise'
		maskingNoise = whiteNoise(nStimulusTrainSamples);
	case 'pinkNoise'
		maskingNoise = pinkNoise(nStimulusTrainSamples);
	case 'redNoise'
		maskingNoise = redNoise(nStimulusTrainSamples);
	case 'gaussianNoise'
		maskingNoise = gaussianNoise(nStimulusTrainSamples);
end
maskingNoise = zeroPhaseButterworthBandpass(maskingNoise, samplingRate, noiseRange, 4);

switch stimulusType
    case 'bandlimitedChirp'
        notchedMaskingNoise = circshift(maskingNoise, round(nStimulusTrainSamples/2), 2);
        notchedMaskingNoise = zeroPhaseFlattopBandstop(notchedMaskingNoise, samplingRate, bandstopRange, 1000);
    case 'toneBurst'
        notchedMaskingNoise = circshift(maskingNoise, round(nStimulusTrainSamples/2), 2);
        notchedMaskingNoise = zeroPhaseButterworthNotch(notchedMaskingNoise, samplingRate, toneFrequency, notchBandwidth);
end    

nFadeSamples = round(fadeTime*samplingRate);
fade = tukeywin(2*nFadeSamples, 1)';
maskingNoise(1:nFadeSamples) = maskingNoise(1:nFadeSamples) .* fade(1:nFadeSamples);
maskingNoise(end-nFadeSamples+1:end) = maskingNoise(end-nFadeSamples+1:end) .* fade(nFadeSamples+1:end);
switch stimulusType
    case {'bandlimitedChirp', 'toneBurst'}
        notchedMaskingNoise(1:nFadeSamples) = notchedMaskingNoise(1:nFadeSamples) .* fade(1:nFadeSamples);
        notchedMaskingNoise(end-nFadeSamples+1:end) = notchedMaskingNoise(end-nFadeSamples+1:end) .* fade(nFadeSamples+1:end);
end

% Normalize all signals to 0 dB FS (peak) 

stimulusTrain = stimulusTrain / max(abs(stimulusTrain));
for k = 1:nITD
    delayedStimulusTrain(k, :) = delayedStimulusTrain(k, :) / max(abs(delayedStimulusTrain(k, :)));
end
maskingNoise = maskingNoise / max(abs(maskingNoise));
switch stimulusType
    case {'bandlimitedChirp', 'toneBurst'}
        notchedMaskingNoise = notchedMaskingNoise / max(abs(notchedMaskingNoise));
end

% Analyze signals (stimulus waveform, PSDs, ISI histogram)

figure;
plot(stimulusTimebase, stimulusSignal);
grid on;
xlim([min(stimulusTimebase), max(stimulusTimebase)]);
xlabel('Time t [s]');
ylabel('Amplitude s(t) [1]');
title('Stimulus waveform');

powerSpectralDensity(stimulusTrain, samplingRate, 'figureTitle', 'Stimulus train (PSD)', 'signalDimension', 'FS');
powerSpectralDensity(maskingNoise, samplingRate, 'figureTitle', 'Masking noise (PSD)', 'signalDimension', 'FS');
switch stimulusType
    case {'bandlimitedChirp', 'toneBurst'}
        powerSpectralDensity(notchedMaskingNoise, samplingRate, 'figureTitle', 'Notched masking noise (PSD)', 'signalDimension', 'FS');
end

figure;
[ISIprevalence, ISIbins] = hist(diff(presentationTimes), nStimuli);
bar(ISIbins, ISIprevalence);
grid on;
set(gca, 'xscale', 'log');
set(gca, 'yscale', 'log');
title('ISI histogram');
xlabel('ISI [s]');
ylabel('Prevalence [1]');

% Export signals to wave files

audiowrite('stimuli/triggerTrack.wav', triggerTrack', samplingRate, 'bitsPerSample', bitDepth);
audiowrite('stimuli/maskingNoise.wav', maskingNoise', samplingRate, 'bitsPerSample', bitDepth);
switch stimulusType
    case {'bandlimitedChirp', 'toneBurst'}
        audiowrite('stimuli/notchedMaskingNoise.wav', notchedMaskingNoise', samplingRate, 'bitsPerSample', bitDepth);
end
audiowrite('stimuli/stimulusTrack.wav', stimulusTrain', samplingRate, 'bitsPerSample', bitDepth);
for k = 1:nITD
    audiowrite(sprintf('stimuli/stimulusTrack_ITD%2.1fms.wav', ITD(k)*1e3), delayedStimulusTrain(k, :)', samplingRate, 'bitsPerSample', bitDepth);
end

fprintf('Done.\n');
    