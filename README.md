# Fast AFRR Acquisition

This repository provides a self-contained MATLAB implementation of methods for the fast acquisition and postprocessing of the auditory full-range response (AFRR) using an interleaved deconvolution approach.